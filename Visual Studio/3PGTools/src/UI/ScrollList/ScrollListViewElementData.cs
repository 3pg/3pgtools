﻿using UnityEngine;

namespace _3PG.Tools.UI.ScrollList {
	internal sealed class ScrollListViewElementData<T, K> : object where T : ScrollListViewElement<K> where K : ScrollListViewElementWrapper {
		private readonly Rect _rect;
		private readonly ScrollListView<T, K> _scrollListView;

		private readonly K _value;

		private T _element;

		public ScrollListViewElementData(K value, Rect rect, ScrollListView<T, K> scrollListView) {
			this._value = value;
			this._rect = rect;
			this._scrollListView = scrollListView;
		}

		public void ApplyPhysics(Vector2 contentPosition, Vector2 viewportSize) {
			contentPosition.x = -contentPosition.x;
			Rect contentViewportRect = new Rect(contentPosition, viewportSize);
			if (contentViewportRect.Overlaps(this._rect)) {
				if (this._element == null) {
					this.OnBecameVisible();
				}
			} else {
				if (this._element != null) {
					this.OnBecameInvisible();
				}
			}
		}

		private void OnBecameVisible() {
			this._element = this._scrollListView.GetElementFromPool();
			if (this._element != null) {
				this._element.Load(this._value);
				this._element.RectTransform.anchoredPosition = new Vector2(this._rect.position.x, -this._rect.position.y);
			}
		}

		private void OnBecameInvisible() {
			if (this._element == null) {
				return;
			}
			this._scrollListView.AddElementToPool(this._element);
			this._element = null;
		}
	}
}
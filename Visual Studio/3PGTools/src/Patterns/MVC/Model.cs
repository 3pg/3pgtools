﻿namespace _3PG.Tools.Patterns.MVC {
	/// <summary>
	/// Base Model for the MVC Pattern. Note that this MVC Pattern is based on a FlyWeight approach.
	/// </summary>
	public abstract class Model : object {}
}
﻿namespace _3PG.Tools.Patterns.Strategies {
	/// <summary>
	/// The Strategy base class for the Strategy Pattern.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public abstract class Strategy<T> where T : class {

		/// <summary>
		/// 
		/// </summary>
		/// <param name="obj"></param>
		public abstract void AlgorithmInterface(T obj);

	}

}

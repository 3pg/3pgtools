﻿using System;
using System.Collections.Generic;
using UnityEngine;
using _3PG.Tools.Tweening.Enums;
using _3PG.Tools.Tweening.Internal;

namespace _3PG.Tools.Tweening {
	/// <summary>
	/// 
	/// </summary>
	public class AnimationCurveDatabase : ScriptableObject {

		/// <summary>
		/// 
		/// </summary>
		[SerializeField]
		private List<AnimationCurveDatabaseKeyValue> _animationCurves;

		/// <summary>
		/// 
		/// </summary>
		private Dictionary<string, AnimationCurve> _animationCurvesCache;

		/// <summary>
		/// 
		/// </summary>
		private List<AnimationCurveDatabaseKeyValue> AnimationCurves {
			get {
				if (this._animationCurves == null || this._animationCurves.Count == 0) {
					
				}
				return this._animationCurves;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		private Dictionary<string, AnimationCurve> AnimationCurvesCache {
			get {
				if (Application.isPlaying && this._animationCurvesCache == null) {
					this._animationCurvesCache = new Dictionary<string, AnimationCurve>();
					foreach (AnimationCurveDatabaseKeyValue keyValue in this.AnimationCurves) {
						this._animationCurvesCache.Add(keyValue.Key, keyValue.Value);
					}
				}
				return this._animationCurvesCache;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="animationCurveName"></param>
		/// <returns></returns>
		private AnimationCurve RetriveAnimationCurve(string animationCurveName) {
			if (Application.isPlaying) {
				if (this.AnimationCurvesCache.ContainsKey(animationCurveName)) {
					return this.AnimationCurvesCache[animationCurveName];
				}
			} else {
				foreach (AnimationCurveDatabaseKeyValue keyValue in this.AnimationCurves) {
					if (keyValue.Key == animationCurveName) {
						return keyValue.Value;
					}
				}
			}
			return null;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="easeType"></param>
		/// <returns></returns>
		public static AnimationCurve GetAnimationCurve(EaseType easeType) {
			return AnimationCurveDatabase.GetAnimationCurve(easeType.ToString());
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="animationCurveName"></param>
		/// <returns></returns>
		public static AnimationCurve GetAnimationCurve(string animationCurveName) {
			AnimationCurveDatabase animationCurveDatabase = Resources.Load<AnimationCurveDatabase>("AnimationCurveDatabase");
			if (animationCurveDatabase != null) {
				return animationCurveDatabase.RetriveAnimationCurve(animationCurveName);
			}
			return null;
		}

	}
}
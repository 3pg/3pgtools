﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace _3PG.Tools.UI {

	[RequireComponent(typeof (Canvas), typeof(GraphicRaycaster))]
	public class DragableCanvas : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

		[SerializeField, Range(0, 30)]
		private float _dragDamp;

		private RectTransform _rectTransform;
		private Canvas _canvas;
		private Canvas _rootCanvas;

		private Vector3 _wantedPosition;

		protected RectTransform RectTransform {
			get { return this._rectTransform ?? (this._rectTransform = this.transform as RectTransform); }
		}

		protected Canvas Canvas {
			get { return this._canvas ?? (this._canvas = this.GetComponent<Canvas>()); }
		}

		protected Canvas RootCanvas {
			get { return this._rootCanvas ?? (this._rootCanvas = this.transform.root.GetComponentInChildren<Canvas>(true)); }
		}

		private void Update() {
			if (this._dragDamp > 0) {
				this.RectTransform.localPosition = Vector3.Lerp(this.RectTransform.localPosition, this._wantedPosition, this._dragDamp * Time.deltaTime);
			} else {
				this.RectTransform.localPosition = this._wantedPosition;
			}
		}

		public void OnBeginDrag(PointerEventData eventData) {
			this.Canvas.overrideSorting = true;
			this.Canvas.sortingOrder = int.MaxValue / 2;
			this.OnDragBegin();
		}

		public void OnDrag(PointerEventData eventData) {
			if (this.RootCanvas.renderMode == RenderMode.ScreenSpaceOverlay) {
				this._wantedPosition = Input.mousePosition;
			} else {
				Vector2 pos;
				RectTransformUtility.ScreenPointToLocalPointInRectangle(this.RootCanvas.transform as RectTransform, Input.mousePosition, this.RootCanvas.worldCamera, out pos);
				this._wantedPosition =  this.RootCanvas.transform.TransformPoint(pos);
			}

			if (this.RectTransform.parent != null) {
				this._wantedPosition -= this.RectTransform.parent.position;
			}
			this.OnDrag();
		}

		public void OnEndDrag(PointerEventData eventData) {
			this.Canvas.sortingOrder = 0;
			this.Canvas.overrideSorting = false;
			this._wantedPosition = Vector2.zero;
			this.OnDragEnd();
		}

		protected virtual void OnDragBegin() {}

		protected virtual void OnDrag() {}

		protected virtual void OnDragEnd() {}

	}
}
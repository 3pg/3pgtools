﻿using System;

namespace _3PG.Tools.Tweening.Exceptions {
	/// <summary>
	/// </summary>
	public class TweenException : Exception {
		/// <summary>
		/// </summary>
		private readonly Tween _tween;

		/// <summary>
		/// </summary>
		/// <param name="tween"></param>
		public TweenException(Tween tween) {
			this._tween = tween;
		}

		/// <summary>
		/// </summary>
		public Tween Tween {
			get { return this._tween; }
		}
	}
}
﻿namespace _3PG.Tools.Tweening.Exceptions {
	/// <summary>
	/// </summary>
	public class StartedTweenException : TweenException {
		/// <summary>
		/// </summary>
		/// <param name="tween"></param>
		public StartedTweenException(Tween tween) : base(tween) {}

		/// <summary>
		/// </summary>
		public override string Message {
			get {
				return string.Format("The Tween with Guid: \"{0}\" is already started and can not be modified.", this.Tween.Guid);
			}
		}
	}
}
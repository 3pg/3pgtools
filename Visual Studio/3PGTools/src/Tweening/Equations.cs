﻿using UnityEngine;

namespace _3PG.Tools.Tweening {
	/// <summary>
	/// </summary>
	public static class Equations {
		#region Expo

		/// <summary>
		/// Easing equation function for an exponential (2^p) easing out:
		/// decelerating from zero velocity.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float ExpoEaseOut(float p) {
			return p == 1 ? 1 : -Mathf.Pow(2, -10 * p) + 1;
		}

		/// <summary>
		/// Easing equation function for an exponential (2^p) easing in:
		/// accelerating from zero velocity.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float ExpoEaseIn(float p) {
			return p == 0 ? 0 : Mathf.Pow(2, 10 * (p - 1));
		}

		/// <summary>
		/// Easing equation function for an exponential (2^p) easing in/out:
		/// acceleration until halfway, then deceleration.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float ExpoEaseInOut(float p) {
			if (p == 0) {
				return 0;
			}
			if (p == 1) {
				return 1;
			}

			if ((p /= 0.5f) < 1) {
				return 0.5f * Mathf.Pow(2, 10 * (p - 1));
			}
			return 0.5f * (-Mathf.Pow(2, -10 * --p) + 2);
		}

		/// <summary>
		/// Easing equation function for an exponential (2^p) easing out/in:
		/// deceleration until halfway, then acceleration.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float ExpoEaseOutIn(float p) {
			return p < 0.5f ? Equations.ExpoEaseOut(p * 2) : Equations.ExpoEaseIn(p * 2 - 1);
		}

		#endregion

		#region Circular

		/// <summary>
		/// Easing equation function for a circular (sqrt(1-p^2)) easing out:
		/// decelerating from zero velocity.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float CircEaseOut(float p) {
			return Mathf.Sqrt(1 - (p = p - 1) * p);
		}

		/// <summary>
		/// Easing equation function for a circular (sqrt(1-p^2)) easing in:
		/// accelerating from zero velocity.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float CircEaseIn(float p) {
			return -1 * (Mathf.Sqrt(1 - (p /= 1) * p) - 1);
		}

		/// <summary>
		/// Easing equation function for a circular (sqrt(1-p^2)) easing in/out:
		/// acceleration until halfway, then deceleration.
		/// </summary>
		/// <param name="t">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float CircEaseInOut(float t) {
			if ((t /= 0.5f) < 1) {
				return -0.5f * (Mathf.Sqrt(1 - t * t) - 1);
			}
			return 0.5f * (Mathf.Sqrt(1 - (t -= 2) * t) + 1);
		}

		/// <summary>
		/// Easing equation function for a circular (sqrt(1-p^2)) easing out/in:
		/// deceleration until halfway, then acceleration.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float CircEaseOutIn(float p) {
			return p < 0.5f ? Equations.CircEaseOut(p * 2) : Equations.CircEaseIn(p * 2 - 1);
		}

		#endregion

		#region Quad

		/// <summary>
		/// Easing equation function for a quadratic (p^2) easing out:
		/// decelerating from zero velocity.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float QuadEaseOut(float p) {
			return -1 * p * (p - 2);
		}

		/// <summary>
		/// Easing equation function for a quadratic (p^2) easing in:
		/// accelerating from zero velocity.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float QuadEaseIn(float p) {
			return p * p;
		}

		/// <summary>
		/// Easing equation function for a quadratic (p^2) easing in/out:
		/// acceleration until halfway, then deceleration.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float QuadEaseInOut(float p) {
			if ((p /= 0.5f) < 1) {
				return 0.5f * p * p;
			}
			return -0.5f * (--p * (p - 2) - 1);
		}

		/// <summary>
		/// Easing equation function for a quadratic (p^2) easing out/in:
		/// deceleration until halfway, then acceleration.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float QuadEaseOutIn(float p) {
			return p < 0.5f ? Equations.QuadEaseOut(p * 2) : Equations.QuadEaseIn(p * 2 - 1);
		}

		#endregion

		#region Sine

		/// <summary>
		/// Easing equation function for a sinusoidal (sin(p)) easing out:
		/// decelerating from zero velocity.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float SineEaseOut(float p) {
			return Mathf.Sin(p * (Mathf.PI / 2));
		}

		/// <summary>
		/// Easing equation function for a sinusoidal (sin(p)) easing in:
		/// accelerating from zero velocity.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float SineEaseIn(float p) {
			return -1 * Mathf.Cos(p * (Mathf.PI / 2)) + 1;
		}

		/// <summary>
		/// Easing equation function for a sinusoidal (sin(p)) easing in/out:
		/// acceleration until halfway, then deceleration.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float SineEaseInOut(float p) {
			if ((p /= 0.5f) < 1) {
				return 0.5f * Mathf.Sin(Mathf.PI * p / 2);
			}
			return -0.5f * (Mathf.Cos(Mathf.PI * --p / 2) - 2);
		}

		/// <summary>
		/// Easing equation function for a sinusoidal (sin(p)) easing out/in:
		/// deceleration until halfway, then acceleration.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float SineEaseOutIn(float p) {
			return p < 0.5f ? Equations.SineEaseOut(p * 2) : Equations.SineEaseIn(p * 2 - 1);
		}

		#endregion

		#region Cubic

		/// <summary>
		/// Easing equation function for a cubic (p^3) easing out:
		/// decelerating from zero velocity.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float CubicEaseOut(float p) {
			return (p = p - 1) * p * p + 1;
		}

		/// <summary>
		/// Easing equation function for a cubic (p^3) easing in:
		/// accelerating from zero velocity.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float CubicEaseIn(float p) {
			return p * p * p;
		}

		/// <summary>
		/// Easing equation function for a cubic (p^3) easing in/out:
		/// acceleration until halfway, then deceleration.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float CubicEaseInOut(float p) {
			if ((p /= 0.5f) < 1) {
				return 0.5f * p * p * p;
			}
			return 0.5f * ((p -= 2) * p * p + 2);
		}

		/// <summary>
		/// Easing equation function for a cubic (p^3) easing out/in:
		/// deceleration until halfway, then acceleration.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float CubicEaseOutIn(float p) {
			return p < 0.5f ? Equations.CubicEaseOut(p * 2) : Equations.CubicEaseIn(p * 2 - 1);
		}

		#endregion

		#region Quartic

		/// <summary>
		/// Easing equation function for a quartic (p^4) easing out:
		/// decelerating from zero velocity.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float QuartEaseOut(float p) {
			return -1 * ((p = p - 1) * p * p * p - 1);
		}

		/// <summary>
		/// Easing equation function for a quartic (p^4) easing in:
		/// accelerating from zero velocity.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float QuartEaseIn(float p) {
			return p * p * p * p;
		}

		/// <summary>
		/// Easing equation function for a quartic (p^4) easing in/out:
		/// acceleration until halfway, then deceleration.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float QuartEaseInOut(float p) {
			if ((p /= 0.5f) < 1) {
				return 0.5f * p * p * p * p;
			}
			return -0.5f * ((p -= 2) * p * p * p - 2);
		}

		/// <summary>
		/// Easing equation function for a quartic (p^4) easing out/in:
		/// deceleration until halfway, then acceleration.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float QuartEaseOutIn(float p) {
			return p < 0.5f ? Equations.QuartEaseOut(p * 2) : Equations.QuartEaseIn(p * 2 - 1);
		}

		#endregion

		#region Quintic

		/// <summary>
		/// Easing equation function for a quintic (p^5) easing out:
		/// decelerating from zero velocity.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float QuintEaseOut(float p) {
			return (p = p - 1) * p * p * p * p + 1;
		}

		/// <summary>
		/// Easing equation function for a quintic (p^5) easing in:
		/// accelerating from zero velocity.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float QuintEaseIn(float p) {
			return p * p * p * p * p;
		}

		/// <summary>
		/// Easing equation function for a quintic (p^5) easing in/out:
		/// acceleration until halfway, then deceleration.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float QuintEaseInOut(float p) {
			if ((p /= 0.5f) < 1) {
				return 0.5f * p * p * p * p * p;
			}
			return 0.5f * ((p -= 2) * p * p * p * p + 2);
		}

		/// <summary>
		/// Easing equation function for a quintic (p^5) easing out/in:
		/// deceleration until halfway, then acceleration.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float QuintEaseOutIn(float p) {
			return p < 0.5f ? Equations.QuintEaseOut(p * 2) : Equations.QuintEaseIn(p * 2 - 1);
		}

		#endregion

		#region Elastic

		/// <summary>
		/// Easing equation function for an elastic (exponentially decaying sine wave) easing out:
		/// decelerating from zero velocity.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float ElasticEaseOut(float p) {
			if (p == 1) {
				return 1;
			}
			return Mathf.Pow(2, -10 * p) * Mathf.Sin((p - 0.075f) * (2 * Mathf.PI) / 0.3f) + 1;
		}

		/// <summary>
		/// Easing equation function for an elastic (exponentially decaying sine wave) easing in:
		/// accelerating from zero velocity.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float ElasticEaseIn(float p) {
			if (p == 1) {
				return 1;
			}
			return -(Mathf.Pow(2, 10 * (p -= 1)) * Mathf.Sin((p - 0.075f) * (2 * Mathf.PI) / 0.3f));
		}

		/// <summary>
		/// Easing equation function for an elastic (exponentially decaying sine wave) easing in/out:
		/// acceleration until halfway, then deceleration.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float ElasticEaseInOut(float p) {
			if ((p /= 0.5f) == 2) {
				return 1;
			}
			if (p < 1) {
				return -0.5f * (Mathf.Pow(2, 10 * (p -= 1)) * Mathf.Sin((p - 0.1125f) * (2 * Mathf.PI) / 0.45f));
			}
			return Mathf.Pow(2, -10 * (p -= 1)) * Mathf.Sin((p - 0.1125f) * (2 * Mathf.PI) / 0.45f) * 0.5f + 1;
		}

		/// <summary>
		/// Easing equation function for an elastic (exponentially decaying sine wave) easing out/in:
		/// deceleration until halfway, then acceleration.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float ElasticEaseOutIn(float p) {
			return p < 0.5f ? Equations.ElasticEaseOut(p * 2) : Equations.ElasticEaseIn(p * 2 - 1);
		}

		#endregion

		#region Bounce

		/// <summary>
		/// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing out:
		/// decelerating from zero velocity.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float BounceEaseOut(float p) {
			if (p < 1 / 2.75f) {
				return 7.5625f * p * p;
			}
			if (p < 2 / 2.75f) {
				return 7.5625f * (p -= 1.5f / 2.75f) * p + 0.75f;
			}
			if (p < 2.5f / 2.75f) {
				return 7.5625f * (p -= 2.25f / 2.75f) * p + 0.9375f;
			}
			return 7.5625f * (p -= 2.625f / 2.75f) * p + 0.984375f;
		}

		/// <summary>
		/// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing in:
		/// accelerating from zero velocity.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float BounceEaseIn(float p) {
			return 1 - Equations.BounceEaseOut(1 - p);
		}

		/// <summary>
		/// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing in/out:
		/// acceleration until halfway, then deceleration.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float BounceEaseInOut(float p) {
			if (p < 0.5f) {
				return Equations.BounceEaseIn(p * 2) * 0.5f;
			}
			return Equations.BounceEaseOut(p * 2 - 1) * 0.5f + 0.5f;
		}

		/// <summary>
		/// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing out/in:
		/// deceleration until halfway, then acceleration.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float BounceEaseOutIn(float p) {
			return p < 0.5f ? Equations.BounceEaseOut(p * 2) : Equations.BounceEaseIn(p * 2 - 1);
		}

		#endregion

		#region Back

		/// <summary>
		/// Easing equation function for a back (overshooting cubic easing: (s+1)*p^3 - s*p^2) easing out:
		/// decelerating from zero velocity.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float BackEaseOut(float p) {
			return (p = p - 1) * p * ((1.70158f + 1) * p + 1.70158f) + 1;
		}

		/// <summary>
		/// Easing equation function for a back (overshooting cubic easing: (s+1)*p^3 - s*p^2) easing in:
		/// accelerating from zero velocity.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float BackEaseIn(float p) {
			return p * p * ((1.70158f + 1) * p - 1.70158f);
		}

		/// <summary>
		/// Easing equation function for a back (overshooting cubic easing: (s+1)*p^3 - s*p^2) easing in/out:
		/// acceleration until halfway, then deceleration.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float BackEaseInOut(float p) {
			float s = 1.70158f;
			if ((p /= 0.5f) < 1) {
				return 0.5f * (p * p * (((s *= 1.525f) + 1) * p - s));
			}
			return 0.5f * ((p -= 2) * p * (((s *= 1.525f) + 1) * p + s) + 2);
		}

		/// <summary>
		/// Easing equation function for a back (overshooting cubic easing: (s+1)*p^3 - s*p^2) easing out/in:
		/// deceleration until halfway, then acceleration.
		/// </summary>
		/// <param name="p">Current progress.</param>
		/// <returns>The correct value.</returns>
		public static float BackEaseOutIn(float p) {
			return p < 0.5f ? Equations.BackEaseOut(p * 2) : Equations.BackEaseIn(p * 2 - 1);
		}

		#endregion
	}
}
﻿using UnityEngine;

namespace _3PG.Tools.Patterns.MVC {
	/// <summary>
	/// Base View for the MVC Pattern.
	/// </summary>
	/// <typeparam name="T">The Model Type.</typeparam>
	public abstract class View<T> : MonoBehaviour where T : Model {
		/// <summary>
		/// The FlyWeight instance of the Model.
		/// </summary>
		private T _model;

		/// <summary>
		/// Sets the FlyWeight Model instance.
		/// </summary>
		/// <param name="model">The FlyWeight Model instance.</param>
		public void SetModel(T model) {
			this._model = model;
		}

		/// <summary>
		/// Gets the current Model instance.
		/// </summary>
		/// <returns>The Model attached to this instance.</returns>
		public T GetModel() {
			return this._model;
		}
	}
}
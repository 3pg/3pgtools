﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _3PG.Tools.Patterns.Toolboxies;

namespace _3PG.Tools.Tweening {
	/// <summary>
	/// </summary>
	public class Tweener : MonoBehaviour, ISingleton {
		/// <summary>
		/// </summary>
		private readonly LinkedList<Tween> _coroutineTweens;

		/// <summary>
		/// </summary>
		private readonly LinkedList<Tween> _timeScaleTweens;

		/// <summary>
		/// </summary>
		private readonly Dictionary<Guid, Tween> _tweens;

		/// <summary>
		/// </summary>
		public Tweener() {
			this._tweens = new Dictionary<Guid, Tween>();
			this._timeScaleTweens = new LinkedList<Tween>();
			this._coroutineTweens = new LinkedList<Tween>();
		}

		/// <summary>
		/// </summary>
		/// <param name="guid"></param>
		/// <returns></returns>
		public Tween GetTween(Guid guid) {
			if (this._tweens.ContainsKey(guid)) {
				return this._tweens[guid];
			}
			return null;
		}

		/// <summary>
		/// </summary>
		/// <returns></returns>
		public IEnumerable<Tween> GetTweens() {
			return this._tweens.Values;
		}

		/// <summary>
		/// </summary>
		public void CancelAll() {
			this._tweens.Clear();
		}

		/// <summary>
		/// </summary>
		/// <param name="tween"></param>
		internal void AddTween(Tween tween) {
			this._tweens.Add(tween.Guid, tween);
			if (tween.UseTimeScale) {
				this._timeScaleTweens.AddLast(tween);
			} else {
				this._coroutineTweens.AddLast(tween);
			}
		}

		/// <summary>
		/// </summary>
		private void Start() {
			//this.StartCoroutine(this.DoCoroutineUpdate());
		}

		/// <summary>
		/// </summary>
		private void Update() {
			this.DoUpdate(this._timeScaleTweens, Time.deltaTime);
		}

		/// <summary>
		/// </summary>
		/// <returns></returns>
		private IEnumerator DoCoroutineUpdate() {
			while (true) {
				// This is a simulation of the Unity update but instead of using the Time.deltaTime we use a forced deltaTime
				// calculation to ignore the Time.timeScale.
				float lastTime = Time.time;
				yield return new WaitForSeconds(0.02f);
				this.DoUpdate(this._coroutineTweens, Time.time - lastTime);
			}
		}

		/// <summary>
		/// </summary>
		/// <param name="linkedList"></param>
		/// <param name="deltaTime"></param>
		private void DoUpdate(LinkedList<Tween> linkedList, float deltaTime) {
			LinkedListNode<Tween> interatedNode = linkedList.First;
			// Interate over the linked list.
			while (interatedNode != null) {
				Tween tween = interatedNode.Value;
				tween.Update(deltaTime);
				// If the tween is already finished removed it from the linked list and from the cache dictionary.
				if (false/*tween.IsCompleted*/) {
					LinkedListNode<Tween> nextNode = interatedNode.Next;
					linkedList.Remove(interatedNode);
					this._tweens.Remove(tween.Guid);
					interatedNode = nextNode;
				} else {
					interatedNode = interatedNode.Next;
				}
			}
		}
	}
}
﻿using System;
using System.Collections;
using UnityEngine;
using _3PG.Tools.Patterns.Toolboxies;

namespace _3PG.Tools {
	public class Coroutiner : MonoBehaviour, ISingleton {

		public Coroutine WaitForSeconds(float seconds, Action callback) {
			return this.StartCoroutine(this.DoYieldInstruction(new WaitForSeconds(seconds), callback));
		}

		public Coroutine WaitForEndOfFrame(Action callback) {
			return this.StartCoroutine(this.DoYieldInstruction(new WaitForEndOfFrame(), callback));
		}

		public Coroutine WaitForFixedUpdate(Action callback) {
			return this.StartCoroutine(this.DoYieldInstruction(new WaitForFixedUpdate(), callback));
		}

		public Coroutine WaitForSecondsRealtime(float seconds, Action callback) {
			return this.StartCoroutine(this.DoCustomYieldInstruction(new WaitForSecondsRealtime(seconds), callback));
		}

		public Coroutine WaitUntil(Func<bool> predicate, Action callback) {
			return this.StartCoroutine(this.DoCustomYieldInstruction(new WaitUntil(predicate), callback));
		}

		public Coroutine WaitWhile(Func<bool> predicate, Action callback) {
			return this.StartCoroutine(this.DoCustomYieldInstruction(new WaitWhile(predicate), callback));
		}

		private IEnumerator DoYieldInstruction(YieldInstruction yieldInstruction, Action callback) {
			yield return yieldInstruction;
			if (callback != null) {
				callback();
			}
		}

		private IEnumerator DoCustomYieldInstruction(CustomYieldInstruction customYieldInstruction, Action callback) {
			yield return customYieldInstruction;
			if (callback != null) {
				callback();
			}
		}

	}
}
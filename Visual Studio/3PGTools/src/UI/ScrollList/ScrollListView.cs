﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace _3PG.Tools.UI.ScrollList {

	[RequireComponent(typeof(ScrollRect))]
	public abstract class ScrollListView<T, K> : MonoBehaviour where T : ScrollListViewElement<K> where K : ScrollListViewElementWrapper {

		[SerializeField]
		private T _element;
		[SerializeField]
		private int _poolSize;

		private List<T> _elementList;
		private Queue<T> _elementPoolQueue;
		private List<ScrollListViewElementData<T, K>> _dataList;

		private ScrollRect _scrollRect;
		private LayoutElement _contentLayoutElement;

		private List<T> ElementList
		{
			get { return this._elementList ?? (this._elementList = new List<T>()); }
		}

		private Queue<T> ElementPoolQueue
		{
			get
			{
				if (this._elementPoolQueue != null) {
					return this._elementPoolQueue;
				}
				this._elementPoolQueue = new Queue<T>();
				this.ElementList.Add(this._element);
				this._elementPoolQueue.Enqueue(this._element);
				for (int i = 0; i < this._poolSize - 1; i++) {
					T element = Object.Instantiate(this._element);
					element.transform.SetParent(this.ScrollRect.content, false);
					this.ElementList.Add(element);
					this._elementPoolQueue.Enqueue(element);
				}
				return this._elementPoolQueue;
			}
		}

		private List<ScrollListViewElementData<T, K>> DataList
		{
			get { return this._dataList ?? (this._dataList = new List<ScrollListViewElementData<T, K>>()); }
		}

		private ScrollRect ScrollRect
		{
			get { return this._scrollRect ?? (this._scrollRect = this.GetComponent<ScrollRect>()); }
		}

		private LayoutElement ContentLayoutElement
		{
			get
			{
				if (this._contentLayoutElement == null && this.ScrollRect.content != null) {
					this._contentLayoutElement = this.ScrollRect.content.GetComponent<LayoutElement>();
				}
				return this._contentLayoutElement;
			}
		}

		protected virtual void FixedUpdate() {
			for (int i = 0; i < this.DataList.Count; i++) {
				this.DataList[i].ApplyPhysics(this.ScrollRect.content.anchoredPosition, this.ScrollRect.viewport.rect.size);
			}
		}

		public void Add(K wrapper) {
			Rect elementRect = this._element.RectTransform.rect;
			float x = this.ScrollRect.horizontal ? elementRect.width * this.DataList.Count : 0;
			float y = this.ScrollRect.vertical ? elementRect.height * this.DataList.Count : 0;
			Rect dataRect = new Rect(x, y, elementRect.width, elementRect.height);
			ScrollListViewElementData<T, K> data = new ScrollListViewElementData<T, K>(wrapper, dataRect, this);
			this.DataList.Add(data);
			this.ContentLayoutElement.minWidth += this.ScrollRect.horizontal ? elementRect.width : 0;
			this.ContentLayoutElement.minHeight += this.ScrollRect.vertical ? elementRect.height : 0;
		}

		private void Clear() {
			this.ElementPoolQueue.Clear();
			this.DataList.Clear();
			for (int i = 0; i < this.ElementList.Count; i++) {
				this.AddElementToPool(this.ElementList[i]);
			}
		}

		public void AddElementToPool(T element) {
			this.ElementPoolQueue.Enqueue(element);
			element.OnElementAddedToPool();
		}

		public T GetElementFromPool() {
			if (this.ElementPoolQueue.Count <= 0) {
				return null;
			}
			T element = this.ElementPoolQueue.Dequeue();
			element.OnElementRemovedFromPool();
			return element;
		}
	}
}

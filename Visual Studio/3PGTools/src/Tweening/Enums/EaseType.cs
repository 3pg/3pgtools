﻿namespace _3PG.Tools.Tweening.Enums {
	/// <summary>
	/// </summary>
	public enum EaseType {
		/// <summary>
		/// Easing equation function for a simple linear tweening, with no easing.
		/// </summary>
		Linear,

		/// <summary>
		/// Easing equation function for a quadratic (p^2) easing out:
		/// decelerating from zero velocity.
		/// </summary>
		QuadEaseOut,
		/// <summary>
		/// Easing equation function for a quadratic (p^2) easing in:
		/// accelerating from zero velocity.
		/// </summary>
		QuadEaseIn,
		/// <summary>
		/// Easing equation function for a quadratic (p^2) easing in/out:
		/// acceleration until halfway, then deceleration.
		/// </summary>
		QuadEaseInOut,
		/// <summary>
		/// Easing equation function for a quadratic (p^2) easing out/in:
		/// deceleration until halfway, then acceleration.
		/// </summary>
		QuadEaseOutIn,

		/// <summary>
		/// Easing equation function for an exponential (2^p) easing out:
		/// decelerating from zero velocity.
		/// </summary>
		ExpoEaseOut,
		/// <summary>
		/// Easing equation function for an exponential (2^p) easing in:
		/// accelerating from zero velocity.
		/// </summary>
		ExpoEaseIn,
		/// <summary>
		/// Easing equation function for an exponential (2^p) easing in/out:
		/// acceleration until halfway, then deceleration.
		/// </summary>
		ExpoEaseInOut,
		/// <summary>
		/// Easing equation function for an exponential (2^p) easing out/in:
		/// deceleration until halfway, then acceleration.
		/// </summary>
		ExpoEaseOutIn,

		/// <summary>
		/// Easing equation function for a cubic (p^3) easing out:
		/// decelerating from zero velocity.
		/// </summary>
		CubicEaseOut,
		/// <summary>
		/// Easing equation function for a cubic (p^3) easing in:
		/// accelerating from zero velocity.
		/// </summary>
		CubicEaseIn,
		/// <summary>
		/// Easing equation function for a cubic (p^3) easing in/out:
		/// acceleration until halfway, then deceleration.
		/// </summary>
		CubicEaseInOut,
		/// <summary>
		/// Easing equation function for a cubic (p^3) easing out/in:
		/// deceleration until halfway, then acceleration.
		/// </summary>
		CubicEaseOutIn,

		/// <summary>
		/// Easing equation function for a quartic (p^4) easing out:
		/// decelerating from zero velocity.
		/// </summary>
		QuartEaseOut,
		/// <summary>
		/// Easing equation function for a quartic (p^4) easing in:
		/// accelerating from zero velocity.
		/// </summary>
		QuartEaseIn,
		/// <summary>
		/// Easing equation function for a quartic (p^4) easing in/out:
		/// acceleration until halfway, then deceleration.
		/// </summary>
		QuartEaseInOut,
		/// <summary>
		/// Easing equation function for a quartic (p^4) easing out/in:
		/// deceleration until halfway, then acceleration.
		/// </summary>
		QuartEaseOutIn,

		/// <summary>
		/// Easing equation function for a quintic (p^5) easing out:
		/// decelerating from zero velocity.
		/// </summary>
		QuintEaseOut,
		/// <summary>
		/// Easing equation function for a quintic (p^5) easing in:
		/// accelerating from zero velocity.
		/// </summary>
		QuintEaseIn,
		/// <summary>
		/// Easing equation function for a quintic (p^5) easing in/out:
		/// acceleration until halfway, then deceleration.
		/// </summary>
		QuintEaseInOut,
		/// <summary>
		/// Easing equation function for a quintic (p^5) easing out/in:
		/// deceleration until halfway, then acceleration.
		/// </summary>
		QuintEaseOutIn,

		/// <summary>
		/// Easing equation function for a circular (sqrt(1-p^2)) easing out:
		/// decelerating from zero velocity.
		/// </summary>
		CircEaseOut,
		/// <summary>
		/// Easing equation function for a circular (sqrt(1-p^2)) easing in:
		/// accelerating from zero velocity. 
		/// </summary>
		CircEaseIn,
		/// <summary>
		/// Easing equation function for a circular (sqrt(1-p^2)) easing in/out:
		/// acceleration until halfway, then deceleration.
		/// </summary>
		CircEaseInOut,
		/// <summary>
		/// Easing equation function for a circular (sqrt(1-p^2)) easing out/in:
		/// deceleration until halfway, then acceleration.
		/// </summary>
		CircEaseOutIn,

		/// <summary>
		/// Easing equation function for a sinusoidal (sin(p)) easing out:
		/// decelerating from zero velocity.
		/// </summary>
		SineEaseOut,
		/// <summary>
		/// Easing equation function for a sinusoidal (sin(p)) easing in:
		/// accelerating from zero velocity.
		/// </summary>
		SineEaseIn,
		/// <summary>
		/// Easing equation function for a sinusoidal (sin(p)) easing in/out:
		/// acceleration until halfway, then deceleration.
		/// </summary>
		SineEaseInOut,
		/// <summary>
		/// Easing equation function for a sinusoidal (sin(p)) easing out/in:
		/// deceleration until halfway, then acceleration.
		/// </summary>
		SineEaseOutIn,

		/// <summary>
		/// Easing equation function for an elastic (exponentially decaying sine wave) easing out:
		/// decelerating from zero velocity.
		/// </summary>
		ElasticEaseOut,
		/// <summary>
		/// Easing equation function for an elastic (exponentially decaying sine wave) easing in:
		/// accelerating from zero velocity.
		/// </summary>
		ElasticEaseIn,
		/// <summary>
		/// Easing equation function for an elastic (exponentially decaying sine wave) easing in/out:
		/// acceleration until halfway, then deceleration.
		/// </summary>
		ElasticEaseInOut,
		/// <summary>
		/// Easing equation function for an elastic (exponentially decaying sine wave) easing out/in:
		/// deceleration until halfway, then acceleration.
		/// </summary>
		ElasticEaseOutIn,

		/// <summary>
		/// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing out:
		/// decelerating from zero velocity.
		/// </summary>
		BounceEaseOut,
		/// <summary>
		/// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing in:
		/// accelerating from zero velocity.
		/// </summary>
		BounceEaseIn,
		/// <summary>
		/// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing in/out:
		/// acceleration until halfway, then deceleration.
		/// </summary>
		BounceEaseInOut,
		/// <summary>
		/// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing out/in:
		/// deceleration until halfway, then acceleration.
		/// </summary>
		BounceEaseOutIn,

		/// <summary>
		/// Easing equation function for a back (overshooting cubic easing: (s+1)*p^3 - s*p^2) easing out:
		/// decelerating from zero velocity.
		/// </summary>
		BackEaseOut,
		/// <summary>
		/// Easing equation function for a back (overshooting cubic easing: (s+1)*p^3 - s*p^2) easing in:
		/// accelerating from zero velocity.
		/// </summary>
		BackEaseIn,
		/// <summary>
		/// Easing equation function for a back (overshooting cubic easing: (s+1)*p^3 - s*p^2) easing in/out:
		/// acceleration until halfway, then deceleration.
		/// </summary>
		BackEaseInOut,
		/// <summary>
		/// Easing equation function for a back (overshooting cubic easing: (s+1)*p^3 - s*p^2) easing out/in:
		/// deceleration until halfway, then acceleration.
		/// </summary>
		BackEaseOutIn
	}
}
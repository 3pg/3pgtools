﻿using UnityEngine;

namespace _3PG.Tools.Patterns.MVC {
	/// <summary>
	/// Base Controller for the MVC Pattern.
	/// </summary>
	/// <typeparam name="T">The Model Type.</typeparam>
	public abstract class Controller<T> : MonoBehaviour where T : Model {
		/// <summary>
		/// The View instance of this Controller.
		/// </summary>
		private View<T> _view;

		/// <summary>
		/// Gets the current Model instance.
		/// </summary>
		/// <returns>The Model attached to this instance.</returns>
		public T GetModel() {
			return this.GetView().GetModel();
		}

		/// <summary>
		/// Gets the current View instance.
		/// </summary>
		/// <returns>The View attached to this instance.</returns>
		protected View<T> GetView() {
			return this._view ?? (this._view = this.GetComponent<View<T>>());
		}
	}
}
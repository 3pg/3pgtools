﻿using UnityEngine;

namespace _3PG.Tools.UI.ScrollList {
	[RequireComponent(typeof (RectTransform))]
	public abstract class ScrollListViewElement<T> : MonoBehaviour where T : ScrollListViewElementWrapper {
		private RectTransform _rectTransform;

		public RectTransform RectTransform {
			get { return this._rectTransform ?? (this._rectTransform = this.transform as RectTransform); }
		}

		public void Load(T data) {
			this.OnLoad(data);
		}

		protected abstract void OnLoad(T data);

		public abstract void OnElementAddedToPool();
		public abstract void OnElementRemovedFromPool();
	}
}
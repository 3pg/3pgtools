﻿using UnityEngine;

namespace _3PG.Tools.Tweening.Extensions {
	/// <summary>
	/// </summary>
	public static class TransformTweeningExtension {
		/// <summary>
		/// </summary>
		/// <param name="target"></param>
		/// <param name="to"></param>
		/// <param name="time"></param>
		/// <returns></returns>
		public static Tween MoveTo(this Transform target, Vector3 to, float time) {
			return target.MoveFromTo(target.transform.position, to, time);
		}

		/// <summary>
		/// </summary>
		/// <param name="target"></param>
		/// <param name="from"></param>
		/// <param name="time"></param>
		/// <returns></returns>
		public static Tween MoveFrom(this Transform target, Vector3 from, float time) {
			return target.MoveFromTo(from, target.transform.position, time);
		}

		/// <summary>
		/// </summary>
		/// <param name="target"></param>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="time"></param>
		/// <returns></returns>
		public static Tween MoveFromTo(this Transform target, Vector3 from, Vector3 to, float time) {
			Vector3 diff = to - from;
			Tween tween = Tween.Value(0, 1, time).AddOnTweenUpdate((Tween t) => {
				target.transform.position = from + diff * t.EvaluatedValue;
			});
			return tween;
		}

		/// <summary>
		/// </summary>
		/// <param name="target"></param>
		/// <param name="to"></param>
		/// <param name="time"></param>
		/// <returns></returns>
		public static Tween LocalMoveTo(this Transform target, Vector3 to, float time) {
			return target.LocalMoveFromTo(target.transform.localPosition, to, time);
		}

		/// <summary>
		/// </summary>
		/// <param name="target"></param>
		/// <param name="from"></param>
		/// <param name="time"></param>
		/// <returns></returns>
		public static Tween LocalMoveFrom(this Transform target, Vector3 from, float time) {
			return target.LocalMoveFromTo(from, target.transform.localPosition, time);
		}

		/// <summary>
		/// </summary>
		/// <param name="target"></param>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="time"></param>
		/// <returns></returns>
		public static Tween LocalMoveFromTo(this Transform target, Vector3 from, Vector3 to, float time) {
			Vector3 diff = to - from;
			Tween tween = Tween.Value(0, 1, time).AddOnTweenUpdate((Tween t) => {
				target.transform.localPosition = from + diff * t.EvaluatedValue;
			});
			return tween;
		}

		/// <summary>
		/// </summary>
		/// <param name="target"></param>
		/// <param name="to"></param>
		/// <param name="time"></param>
		/// <returns></returns>
		public static Tween RotateTo(this Transform target, Vector3 to, float time) {
			return target.RotateFromTo(target.transform.eulerAngles, to, time);
		}

		/// <summary>
		/// </summary>
		/// <param name="target"></param>
		/// <param name="from"></param>
		/// <param name="time"></param>
		/// <returns></returns>
		public static Tween RotateFrom(this Transform target, Vector3 from, float time) {
			return target.RotateFromTo(from, target.transform.eulerAngles, time);
		}

		/// <summary>
		/// </summary>
		/// <param name="target"></param>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="time"></param>
		/// <returns></returns>
		public static Tween RotateFromTo(this Transform target, Vector3 from, Vector3 to, float time) {
			Vector3 diff = to - from;
			Tween tween = Tween.Value(0, 1, time).AddOnTweenUpdate((Tween t) => {
				target.transform.rotation = Quaternion.Euler(from + diff * t.EvaluatedValue);
			});
			return tween;
		}

		/// <summary>
		/// </summary>
		/// <param name="target"></param>
		/// <param name="to"></param>
		/// <param name="time"></param>
		/// <returns></returns>
		public static Tween LocalRotateTo(this Transform target, Vector3 to, float time) {
			return target.LocalRotateFromTo(target.transform.localEulerAngles, to, time);
		}

		/// <summary>
		/// </summary>
		/// <param name="target"></param>
		/// <param name="from"></param>
		/// <param name="time"></param>
		/// <returns></returns>
		public static Tween LocalRotateFrom(this Transform target, Vector3 from, float time) {
			return target.LocalRotateFromTo(from, target.transform.localEulerAngles, time);
		}

		/// <summary>
		/// </summary>
		/// <param name="target"></param>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="time"></param>
		/// <returns></returns>
		public static Tween LocalRotateFromTo(this Transform target, Vector3 from, Vector3 to, float time) {
			Vector3 diff = to - from;
			Tween tween = Tween.Value(0, 1, time).AddOnTweenUpdate((Tween t) => {
				target.transform.localRotation = Quaternion.Euler(from + diff * t.EvaluatedValue);
			});
			return tween;
		}

		/// <summary>
		/// </summary>
		/// <param name="target"></param>
		/// <param name="to"></param>
		/// <param name="time"></param>
		/// <returns></returns>
		public static Tween ScaleTo(this Transform target, Vector3 to, float time) {
			return target.ScaleFromTo(target.transform.localScale, to, time);
		}

		/// <summary>
		/// </summary>
		/// <param name="target"></param>
		/// <param name="from"></param>
		/// <param name="time"></param>
		/// <returns></returns>
		public static Tween ScaleFrom(this Transform target, Vector3 from, float time) {
			return target.ScaleFromTo(from, target.transform.localScale, time);
		}

		/// <summary>
		/// </summary>
		/// <param name="target"></param>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="time"></param>
		/// <returns></returns>
		public static Tween ScaleFromTo(this Transform target, Vector3 from, Vector3 to, float time) {
			Vector3 diff = to - from;
			Tween tween = Tween.Value(0, 1, time).AddOnTweenUpdate((Tween t) => {
				target.transform.localScale = from + diff * t.EvaluatedValue;
			});
			return tween;
		}
	}
}
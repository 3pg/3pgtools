﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace _3PG.Tools.Patterns.Toolboxies {
	public class Toolbox {

		/// <summary>
		/// 
		/// </summary>
		private static Dictionary<Type, ISingleton> _singletons;

		/// <summary>
		/// 
		/// </summary>
		private static Dictionary<Type, ISingleton> Singletons {
			get {
				if (Toolbox._singletons == null) {
					Toolbox._singletons = new Dictionary<Type, ISingleton>();
				}
				return Toolbox._singletons;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public static T GetInstance<T>() where T : class, ISingleton, new() {
			Type type = typeof(T);
			T returnValue = null;
			if (Toolbox.Singletons.ContainsKey(type)) {
				returnValue = Toolbox.Singletons[type] as T;
			}
			else {
				if (type.IsSubclassOf(typeof(Component))) {
					returnValue = GameObject.FindObjectOfType(type) as T;
					if (returnValue == null) {
						GameObject go = new GameObject(type.Name, type);
						returnValue = go.GetComponent<T>();
					}
				} else {
					returnValue = new T();
				}
				Toolbox.Singletons.Add(type, returnValue);
			}
			return returnValue;
		}

	}
}

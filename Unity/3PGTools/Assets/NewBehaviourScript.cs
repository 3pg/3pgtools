﻿using UnityEngine;
using System.Collections;
using _3PG.Tools.Tweening;
using _3PG.Tools.Tweening.Enums;

public class NewBehaviourScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		this.transform.ScaleTo(this.transform.position + Vector3.one * 2, 1).AddOnTweenStart((Tween t) => {
			Debug.LogWarning("Start => \t" + Time.time);
		}).AddOnTweenLoopBegin((Tween t) => {
			Debug.LogWarning("LoopBegin => \t" + Time.time);
		}).AddOnTweenUpdate((Tween t) => {
			Debug.Log("Update => \t" + Time.time);
		}).AddOnTweenLoopEnd((Tween t) => {
			Debug.LogWarning("LoopEnd => \t" + Time.time);
		}).AddOnTweenComplete((Tween t) => {
			Debug.LogWarning("Complete => \t" + Time.time);
		}).SetLoopCount(6).SetLoopType(LoopType.PingPong).Start();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

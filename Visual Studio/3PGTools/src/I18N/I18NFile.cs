﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Security;

namespace _3PG.Tools.I18N {
	public abstract class I18NFile {

		private readonly Dictionary<string, string> _dictionary;

		public I18NFile(string file) {
			this._dictionary = new Dictionary<string, string>();
			this.Load(file);
		}

		public string GetText(string key) {
			return null;
		}

		public string GetText(string key, int value) {
			return this.GetFormatedText(key, value);
		}

		protected abstract string GetFormatedText(string key, int value);

		private void Load(string file) {
			string[] lineSeparators = { "\r\n" };
			string[] lines = file.Split(lineSeparators, StringSplitOptions.None);
			int lastTabCount = 0;
			List<string> groups = new List<string>();
			for (int i = 0; i < lines.Length; i++) {
				int tabCount = this.GetTabCount(lines[i]);
				if (tabCount < lastTabCount) {
					int diff = lastTabCount - tabCount;
					groups.RemoveRange(groups.Count - diff, diff);
				}
				lastTabCount = tabCount;
				string untabedLine = lines[i].Replace("\t", "").Trim();
				// --- If the line ends with ':' means that the line is a group, if not it's a value.
				if (untabedLine.EndsWith(":")) {
					groups.Add(untabedLine);
				} else {
					string fullKey = "";
					foreach (string key in groups) {
						fullKey += key;
					}
					string[] keyValue = untabedLine.Split(':');
					if (keyValue.Length != 2) {
						throw new I18NSyntaxException(i, lines[i]);
					}
					this._dictionary.Add(fullKey + keyValue[0], keyValue[1].TrimStart());
				}
			}
		}

		private int GetTabCount(string value) {
			for (int i = 0; i < value.Length; i++) {
				if (value[i] != '\t') {
					return i;
				}
			}
			return 0;
		}

	}
}
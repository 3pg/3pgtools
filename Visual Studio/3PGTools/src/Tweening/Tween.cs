﻿using System;
using UnityEngine;
using _3PG.Tools.Patterns.Toolboxies;
using _3PG.Tools.Tweening.Enums;
using _3PG.Tools.Tweening.Exceptions;

namespace _3PG.Tools.Tweening {
	/// <summary>
	/// </summary>
	public class Tween : object {
		/// <summary>
		/// </summary>
		/// <param name="tween"></param>
		public delegate void OnTweenCompleteDelegate(Tween tween);

		/// <summary>
		/// </summary>
		/// <param name="tween"></param>
		public delegate void OnTweenLoopBeginDelegate(Tween tween);

		/// <summary>
		/// </summary>
		/// <param name="tween"></param>
		public delegate void OnTweenLoopEndDelegate(Tween tween);

		/// <summary>
		/// </summary>
		/// <param name="tween"></param>
		public delegate void OnTweenStartDelegate(Tween tween);

		/// <summary>
		/// </summary>
		/// <param name="tween"></param>
		public delegate void OnTweenUpdateDelegate(Tween tween);

		/// <summary>
		/// </summary>
		private readonly Guid _guid;

		/// <summary>
		/// </summary>
		private readonly float _from;

		/// <summary>
		/// </summary>
		private readonly float _to;

		/// <summary>
		/// </summary>
		private readonly float _time;


		/// <summary>
		/// </summary>
		private float _delay;

		/// <summary>
		/// </summary>
		private int _loopCount;

		/// <summary>
		/// </summary>
		private LoopType _loopType;

		/// <summary>
		/// </summary>
		private bool _useTimeScale;

		/// <summary>
		/// 
		/// </summary>
		private AnimationCurve _animationCurve;

		/// <summary>
		/// </summary>
		private float _elapsedTime;

		/// <summary>
		/// </summary>
		private bool _isPlaying;

		/// <summary>
		/// </summary>
		private bool _isStarted;

		/// <summary>
		/// </summary>
		private int _loopsCompleted;

		/// <summary>
		/// </summary>
		private bool _alreadyCalledStartCallback;

		/// <summary>
		/// </summary>
		private bool _alreadyCalledLoopBeginCallback;

		/// <summary>
		/// </summary>
		private int _tweenDirection;


		/// <summary>
		/// </summary>
		private Tween() {
			this._guid = new Guid();
			this._useTimeScale = true;
			this._tweenDirection = 1;
			this._animationCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
		}

		/// <summary>
		/// </summary>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="time"></param>
		private Tween(float from, float to, float time) : this() {
			this._from = from;
			this._to = to;
			this._time = time;
		}

		/// <summary>
		/// </summary>
		public Guid Guid {
			get { return this._guid; }
		}

		/// <summary>
		/// </summary>
		public float From {
			get { return this._from; }
		}

		/// <summary>
		/// </summary>
		public float To {
			get { return this._to; }
		}

		/// <summary>
		/// </summary>
		public float Time {
			get { return this._time; }
		}

		/// <summary>
		/// </summary>
		public float Delay {
			get { return this._delay; }
		}

		/// <summary>
		/// </summary>
		public LoopType LoopType {
			get { return this._loopType; }
		}

		/// <summary>
		/// </summary>
		public int LoopCount {
			get { return this._loopCount; }
		}

		/// <summary>
		/// </summary>
		public bool UseTimeScale {
			get { return this._useTimeScale; }
		}

		/// <summary>
		/// </summary>
		public float Progress {
			get { return this._elapsedTime / this._time; }
		}

		public float EvaluatedValue {
			get { return this._animationCurve.Evaluate(this.Progress); }
		}

		/// <summary>
		/// </summary>
		public bool IsPlaying {
			get { return this._isPlaying; }
		}

		/// <summary>
		/// </summary>
		private event OnTweenStartDelegate OnTweenStart;

		/// <summary>
		/// </summary>
		private event OnTweenLoopBeginDelegate OnTweenLoopBegin;

		/// <summary>
		/// </summary>
		private event OnTweenUpdateDelegate OnTweenUpdate;

		/// <summary>
		/// </summary>
		private event OnTweenLoopEndDelegate OnTweenLoopEnd;

		/// <summary>
		/// </summary>
		private event OnTweenCompleteDelegate OnTweenComplete;

		/// <summary>
		/// </summary>
		/// <param name="delay"></param>
		/// <returns></returns>
		public Tween SetDelay(float delay) {
			this.IsTweenStartedAssert();
			this._delay = delay;
			return this;
		}

		/// <summary>
		/// </summary>
		/// <param name="loopType"></param>
		/// <returns></returns>
		public Tween SetLoopType(LoopType loopType) {
			this.IsTweenStartedAssert();
			this._loopType = loopType;
			return this;
		}

		/// <summary>
		/// </summary>
		/// <param name="loopCount"></param>
		/// <returns></returns>
		public Tween SetLoopCount(int loopCount) {
			this.IsTweenStartedAssert();
			this._loopCount = loopCount;
			return this;
		}

		/// <summary>
		/// </summary>
		/// <param name="useTimeScale"></param>
		/// <returns></returns>
		public Tween SetUseTimeScale(bool useTimeScale) {
			this.IsTweenStartedAssert();
			this._useTimeScale = useTimeScale;
			return this;
		}

		/// <summary>
		/// </summary>
		/// <param name="onTweenStartDelegate"></param>
		/// <returns></returns>
		public Tween AddOnTweenStart(OnTweenStartDelegate onTweenStartDelegate) {
			this.IsTweenStartedAssert();
			this.OnTweenStart += onTweenStartDelegate;
			return this;
		}

		/// <summary>
		/// </summary>
		/// <param name="onTweenLoopBeginDelegate"></param>
		/// <returns></returns>
		public Tween AddOnTweenLoopBegin(OnTweenLoopBeginDelegate onTweenLoopBeginDelegate) {
			this.IsTweenStartedAssert();
			this.OnTweenLoopBegin += onTweenLoopBeginDelegate;
			return this;
		}

		/// <summary>
		/// </summary>
		/// <param name="onTweenUpdateDelegate"></param>
		/// <returns></returns>
		public Tween AddOnTweenUpdate(OnTweenUpdateDelegate onTweenUpdateDelegate) {
			this.IsTweenStartedAssert();
			this.OnTweenUpdate += onTweenUpdateDelegate;
			return this;
		}

		/// <summary>
		/// </summary>
		/// <param name="onTweenLoopEndDelegate"></param>
		/// <returns></returns>
		public Tween AddOnTweenLoopEnd(OnTweenLoopEndDelegate onTweenLoopEndDelegate) {
			this.IsTweenStartedAssert();
			this.OnTweenLoopEnd += onTweenLoopEndDelegate;
			return this;
		}

		/// <summary>
		/// </summary>
		/// <param name="onTweenCompleteDelegate"></param>
		/// <returns></returns>
		public Tween AddOnTweenComplete(OnTweenCompleteDelegate onTweenCompleteDelegate) {
			this.IsTweenStartedAssert();
			this.OnTweenComplete += onTweenCompleteDelegate;
			return this;
		}

		/// <summary>
		/// </summary>
		public void Start() {
			this._isStarted = true;
			this._isPlaying = true;
			Toolbox.GetInstance<Tweener>().AddTween(this);
		}

		/// <summary>
		/// </summary>
		public void Play() {
			this._isPlaying = true;
		}

		/// <summary>
		/// </summary>
		public void Pause() {
			this._isPlaying = false;
		}

		/// <summary>
		/// </summary>
		public void Stop() {
			this.Pause();
			this._elapsedTime = 0;
		}

		/// <summary>
		/// </summary>
		/// <param name="deltaTime"></param>
		internal void Update(float deltaTime) {
			if (!this._isPlaying) {
				return;
			}
			if (!this._alreadyCalledStartCallback) {
				this._alreadyCalledStartCallback = true;
				this.OnTweenStart?.Invoke(this);
			}

			this._elapsedTime += deltaTime * this._tweenDirection;

			/*if (!(this._elapsedTime > this.Delay)) {
				return;
			}*/

			if (!this._alreadyCalledLoopBeginCallback) {
				this._alreadyCalledLoopBeginCallback = true;
				this.OnTweenLoopBegin?.Invoke(this);
			}

			if (this.Progress >= 0 && this.Progress <= 1) {
				this.OnTweenUpdate?.Invoke(this);
			} else {
				this.OnTweenLoopEnd?.Invoke(this);
				this._loopsCompleted++;
				if (this._loopsCompleted >= this.LoopCount) {
					this.OnTweenComplete?.Invoke(this);
					this._isPlaying = false;
				} else {
					if (this.LoopType == LoopType.Clamp) {
						this._elapsedTime = this.Delay;
					} else {
						this._elapsedTime = this._tweenDirection == 1 ? this.Time : this.Delay;
						this._tweenDirection = -this._tweenDirection;
					}
					this._alreadyCalledLoopBeginCallback = false;
				}
			}
		}

		/// <summary>
		/// </summary>
		private void IsTweenStartedAssert() {
			if (this._isStarted) {
				throw new StartedTweenException(this);
			}
		}

		/// <summary>
		/// </summary>
		/// <returns></returns>
		public override string ToString() {
			return string.Format("Tween -> GUID: {0} | Progress: {1} | IsPlaying: {2}", this.Guid, this.Progress, this.IsPlaying);
		}

		/// <summary>
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(object obj) {
			if (obj is Tween) {
				return this.GetHashCode() == obj.GetHashCode();
			}
			return false;
		}

		/// <summary>
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode() {
			return this._guid.GetHashCode();
		}

		/// <summary>
		/// Interpolates a value from a point a to a point b in a determined time.
		/// </summary>
		/// <param name="from">Inicial value.</param>
		/// <param name="to">Final value.</param>
		/// <param name="time">Interpolation time.</param>
		/// <returns>The tween new instance.</returns>
		public static Tween Value(float from, float to, float time) {
			return new Tween(from, to, time);
		}
	}
}
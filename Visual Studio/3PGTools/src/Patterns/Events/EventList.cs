﻿using System;
using UnityEngine;

namespace _3PG.Tools.Patterns.Events {
	/// <summary>
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class EventList<T> : object where T : EventArgs {
		/// <summary>
		/// </summary>
		private event EventHandler<T> Listeners;

		/// <summary>
		/// </summary>
		/// <param name="eventHandler"></param>
		public void Insert(EventHandler<T> eventHandler) {
			this.Listeners += eventHandler;
		}

		/// <summary>
		/// </summary>
		/// <param name="eventHandler"></param>
		public void Remove(EventHandler<T> eventHandler) {
			this.Listeners -= eventHandler;
		}

		/// <summary>
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="eventArgs"></param>
		public void Invoke(object sender, T eventArgs) {
			if (this.Listeners != null) {
				try {
					this.Listeners.Invoke(sender, eventArgs);
				} catch (Exception e) {
					Debug.LogException(e);
				}
			}
		}

		/// <summary>
		/// </summary>
		public void Clear() {
			this.Listeners = null;
		}
	}
}
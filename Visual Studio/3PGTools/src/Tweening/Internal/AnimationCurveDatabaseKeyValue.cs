﻿using System;
using UnityEngine;

namespace _3PG.Tools.Tweening.Internal {
	[Serializable]
	internal class AnimationCurveDatabaseKeyValue : object {
		
		/// <summary>
		/// 
		/// </summary>
		[SerializeField]
		private string _key;

		/// <summary>
		/// 
		/// </summary>
		[SerializeField]
		private AnimationCurve _value;

		/// <summary>
		/// Initializes a new instance of the class.
		/// </summary>
		public AnimationCurveDatabaseKeyValue(string key, AnimationCurve value) {
			this._key = key;
			this._value = value;
		}
		
		/// <summary>
		/// 
		/// </summary>
		public string Key {
			get { return this._key; }
		}

		/// <summary>
		/// 
		/// </summary>
		public AnimationCurve Value {
			get { return this._value; }
		}

	}
}
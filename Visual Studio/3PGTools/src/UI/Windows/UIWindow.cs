﻿using System.Runtime.CompilerServices;
using UnityEngine;

namespace _3PG.Tools.UI.Windows {

	[RequireComponent(typeof(Canvas))]
	public abstract class UIWindow<T> : MonoBehaviour where T : UIWindowShowWrapper {

		private static UIWindow<T> _instance;

		[SerializeField]
		private int _layer;

		private T _data;

		public int Layer {
			get { return this._layer; }
		}

		protected T Data {
			get { return this._data; }
		}

		public static void Show(T data) {
			if (UIWindow<T>._instance == null) {
				Canvas rootCanvas = Object.FindObjectOfType<Canvas>();
				UIWindow<T>._instance = rootCanvas.GetComponentInChildren<UIWindow<T>>(true);
			}

			if (UIWindow<T>._instance != null) {
				UIWindow<T>._instance._data = data;
				UIWindow<T>._instance.OnShow();
			}
		}

		public static void Hide() {
			if (UIWindow<T>._instance == null) {
				Canvas rootCanvas = Object.FindObjectOfType<Canvas>();
				UIWindow<T>._instance = rootCanvas.GetComponentInChildren<UIWindow<T>>(true);
			}

			if (UIWindow<T>._instance != null) {
				UIWindow<T>._instance.OnHide();
			}
		}

		private void Init() {
			UIWindow<T>._instance = this;
			this.OnInit();
		}

		protected abstract void OnInit();
		protected abstract void OnShow();
		protected abstract void OnHide();

	}
}
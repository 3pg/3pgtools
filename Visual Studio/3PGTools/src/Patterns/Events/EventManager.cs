﻿using System;
using System.Collections.Generic;

namespace _3PG.Tools.Patterns.Events {

	/// <summary>
	/// 
	/// </summary>
	public static class EventManager {

		/// <summary>
		/// 
		/// </summary>
		private static Dictionary<Type, object> _listeners;

		/// <summary>
		/// 
		/// </summary>
		private static Dictionary<Type, object> Listeners {
			get {
				if (EventManager._listeners == null) {
					EventManager._listeners = new Dictionary<Type, object>();
				}
				return EventManager._listeners;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		private static EventList<T> GetEventList<T>() where T : EventArgs {
			EventList<T> eventList = null;
			Type type = typeof(T);
			if (EventManager.Listeners.ContainsKey(type)) {
				eventList = (EventList<T>)EventManager.Listeners[type];
			} else {
				eventList = new EventList<T>();
				EventManager.Listeners.Add(type, eventList);
			}
			return eventList;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="eventHander"></param>
		public static void Register<T>(EventHandler<T> eventHander) where T : EventArgs {
			EventManager.GetEventList<T>().Insert(eventHander);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="eventHander"></param>
		public static void Unregister<T>(EventHandler<T> eventHander) where T : EventArgs {
			EventManager.GetEventList<T>().Remove(eventHander);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="sender"></param>
		/// <param name="eventArgs"></param>
		public static void Dispatch<T>(object sender, T eventArgs) where T : EventArgs {
			EventManager.GetEventList<T>().Invoke(sender, eventArgs);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		public static void Clear<T>() where T : EventArgs {
			EventManager.GetEventList<T>().Clear();
		}

		/// <summary>
		/// 
		/// </summary>
		public static void Clear() {
			EventManager.Listeners.Clear();
		}

	}
}

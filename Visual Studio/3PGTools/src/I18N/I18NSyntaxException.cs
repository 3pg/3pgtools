﻿using System;

namespace _3PG.Tools.I18N {
	public class I18NSyntaxException : Exception {

		public I18NSyntaxException(int line, string nearTo) : base(String.Format("I18N File syntax error found at line {0} near to \"{1}\"", line, nearTo)) {}

	}
}